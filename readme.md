# Castor PHP assessment

## Notes
Created a package for a polygon manager, handling different polygons and two functions for calculating the area and perimeter.

Started out by creating the Polygon interface which defines the area() and perimeter() functions to be implemented by each shape. Then simply coded against the interface and finally wrote basic tests for each.

Used common practices I'm used to while working. In regards to SOLID principles, the Polygon shapes adheres to the open-closed principle as they're open for extension, but still closed in regards to having to implement the two functions. In the interface both area() and perimeter() has defined return type declarations, which are new in PHP 7 (making it easier to adhere to Liskov substitution).


### Objective:

Write a small PHP class hierarchy. The project will be called Polygon Manager and will help people to calculate basic polygons operations.

The Polygon Manager will take care of these different polygons:

* Triangles

* Rectangles

* Squares

* Five- or more-sided regular polygons.

The Polygon Manager will provide two operations to its end users:
- Calculate the perimeter of the polygon
- Calculate the area of the polygon

Focus on developing an OOP hierarchy to model polygon properties. Pay attention to the use of SOLID principles into your code.

Create automated tests for your main public methods using PhpUnit.

Add a README.md file explaining your architecture and justifiying your choices. Explain how and why you applied certain programming paradigms or SOLID principles.

After you send in your code we will schedule a code review session where you can further clarify your choices and where we can discuss the assessment in detail.

Note: You don’t need this to have a real MVC framework or to provide an external interface where users will enter data and view results. This is beyond the scope of the assessment. You are however free to use any third-party packages as you seem fit.

The assessment will be evaluated based on code structure, cleanliness, scalability, readability, documentation, OOP and knowledge/application of SOLID principles.