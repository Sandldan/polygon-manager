<?php namespace PolygonManager\Shapes;

use PolygonManager\Contracts\Polygon;

class Rectangle implements Polygon
{
    public $length;
    public $height;

    public function __construct($length, $height)
    {
        $this->length = $length;
        $this->height = $height;
    }

    /**
     * Calculate the area of the shape
     *
     * @return float
    */
    public function area() : float
    {
        return $this->length * $this->height;
    }

    /**
     * Calculate the perimeter of the shape
     *
     * @return float
    */
    public function perimeter() : float
    {
        return 2 * ($this->length + $this->height);
    }
}
