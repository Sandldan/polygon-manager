<?php namespace PolygonManager\Shapes;

use PolygonManager\Contracts\Polygon;

class Triangle implements Polygon
{
    public $height;
    public $base;
    public $leftSide;
    public $rightSide;

    public function __construct($height, $base, $leftSide, $rightSide)
    {
        $this->height = $height;
        $this->base = $base;
        $this->leftSide = $leftSide;
        $this->rightSide = $rightSide;
    }

    /**
     * Calculate the area of the shape
     *
     * @return float
    */
    public function area() : float
    {
        return ($this->height * $this->base) / 2;
    }

    /**
     * Calculate the perimeter of the shape
     *
     * @return float
    */
    public function perimeter() : float
    {
        return $this->base + $this->leftSide + $this->rightSide;
    }
}
