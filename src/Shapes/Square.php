<?php namespace PolygonManager\Shapes;

use PolygonManager\Contracts\Polygon;

class Square implements Polygon {
	
	public $length;

	function __construct($length)
	{
		$this->length = $length;
	}

    /**
     * Calculate the area of the shape
     *
     * @return float
    */
	public function area() : float
	{
		return $this->length * $this->length;
	}

    /**
     * Calculate the perimeter of the shape
     *
     * @return float
    */
	public function perimeter() : float
	{
		return 4 * $this->length;
	}

}
