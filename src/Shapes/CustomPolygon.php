<?php namespace PolygonManager\Shapes;

use PolygonManager\Contracts\Polygon;

class CustomPolygon implements Polygon
{
    public $sides;
    public $length;
    public $apothem;

    public function __construct($sides, $length)
    {
        $this->sides = $sides;
        $this->length = $length;
        $this->apothem = ($this->length) / (2 * tan(pi() / $this->sides));
    }

    /**
     * Calculate the area of the shape
     *
     * @return float
    */
    public function area() : float
    {
        return ($this->perimeter() * $this->apothem) / 2;
    }

    /**
     * Calculate the perimeter of the shape
     *
     * @return float
    */
    public function perimeter() : float
    {
        return $this->sides * $this->length;
    }
}
