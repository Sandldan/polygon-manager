<?php namespace PolygonManager\Contracts;

interface Polygon {

	public function area() : float;
	public function perimeter() : float;

}
