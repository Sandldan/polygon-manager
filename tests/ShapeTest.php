<?php

use PHPUnit\Framework\TestCase;
use PolygonManager\Shapes\Square;
use PolygonManager\Shapes\Triangle;
use PolygonManager\Shapes\Rectangle;
use PolygonManager\Shapes\CustomPolygon;

/**
 *   Tests public shape functions
*/
final class ShapeTest extends TestCase
{

    /**
     *   Tests triangle area and perimeter.
    */
    public function testTriangleArea()
    {
        $triangle = new Triangle(5, 10, 4, 4);
        $this->assertEquals($triangle->area(), 25);
    }

    public function testTrianglePerimeter()
    {
        $triangle = new Triangle(5, 10, 4, 4);
        $this->assertEquals($triangle->perimeter(), 18);
    }

    /**
     *   Tests square area and perimeter.
    */
    public function testSquareArea()
    {
        $square = new Square(5);
        $this->assertEquals($square->area(), 25);
    }

    public function testSquarePerimeter()
    {
        $square = new Square(5);
        $this->assertEquals($square->perimeter(), 20);
    }

    /**
     *   Tests rectangle area and perimeter.
    */
    public function testRectangleArea()
    {
        $rectangle = new Rectangle(5, 10);
        $this->assertEquals($rectangle->area(), 50);
    }

    public function testRectanglePerimeter()
    {
        $rectangle = new Rectangle(5, 10);
        $this->assertEquals($rectangle->perimeter(), 30);
    }

    /**
     *   Tests custom polygon area and perimeter.
    */
    public function testCustomPolygonArea()
    {
        $customPolygon = new CustomPolygon(8, 7);
        $this->assertEquals($customPolygon->area(), 236.5929291125633);
    }

    public function testCustomPolygonPerimeter()
    {
        $customPolygon = new CustomPolygon(8, 7);
        $this->assertEquals($customPolygon->perimeter(), 56);
    }
}